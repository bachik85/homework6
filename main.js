$(function () {
    $('a[data-target="anchor"]').bind('click.smoothScroll', function () {
        let target = $(this).attr('href'),
            bl_top = $(target).offset().top;
        $('body, html').animate({scrollTop: bl_top}, 1000);
        return false;
    })
});

//************************** Button to Top **************************************
function backToTop() {
    let button = $('.back-to-top');

    $(window).on('scroll', () => {
        if ($(this).scrollTop() >= 700) {
            button.fadeIn();
        } else {
            button.fadeOut();
        }
    });
    button.on('click', (e) => {
        e.preventDefault();
        $('html').animate({scrollTop: 0}, 1500);
    })
}

backToTop();
//************************* Function slideToggle *******************************
$(document).ready(function () {
    $(".slide-toggle").click(function () {
        $('.top-rated').slideToggle();
    })

})
